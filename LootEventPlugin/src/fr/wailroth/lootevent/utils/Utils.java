package fr.wailroth.lootevent.utils;

import fr.wailroth.lootevent.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import javax.security.auth.login.CredentialException;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 18/08/2020
 * Copyright Alexis Dumain - 2020
 */
public class Utils {

    private static Main main;

    public Utils(Main main) {
        this.main = main;
    }

    public static String color(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }


    //timestamps
    public static int msToDay(long ms) {
        return (int) (ms / (1000 * 60 * 60 * 24) % 7);
    }

    public static int msToHour(long ms) {
        return (int) (ms / (1000 * 60 * 60) % 24);
    }

    public static int msToMinute(long ms) {
        return (int) ((ms / (1000 * 60)) % 60);
    }

    public static int minuteToMs(int minute){
        return minute * 60000;
    }



}
