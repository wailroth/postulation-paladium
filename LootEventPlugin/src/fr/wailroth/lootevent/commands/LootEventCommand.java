package fr.wailroth.lootevent.commands;

import fr.blastfight.api.BigTitle;
import fr.wailroth.lootevent.Main;
import fr.wailroth.lootevent.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigInteger;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 24/08/2020
 * Copyright Alexis Dumain - 2020
 */
public class LootEventCommand implements CommandExecutor {


    private final Main main;

    public LootEventCommand(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {


        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        if (!player.hasPermission(main.getConfigs().getPermission())) {
            player.sendMessage("§cVous n'avez pas la permission d'executer cette commande.");
            return false;
        }

        if (args.length < 1) {
            player.sendMessage("§cMauvaise commande. §8[§b/lootevent §5<start/stop> §8|| §b/lootevent §egive §5ID]");
            return false;
        }


        if (args[0].equalsIgnoreCase("start")) {

            if(main.isEventActivated()){
                player.sendMessage("§cVous ne pouvez pas exectuer cette commande, l'event est activé !");
                return false;
            }

            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                BigTitle.sendTitleToPlayer(p, Utils.color(main.getConfigs().getStartMessageTitle()));
                BigTitle.sendSubtitleToPlayer(p, Utils.color(main.getConfigs().getStartMessage()));
            }
            main.setEventActivated(true);
        }

        if (args[0].equalsIgnoreCase("stop")) {

            if (!main.isEventActivated()) {
                player.sendMessage("§cVous ne pouvez pas exectuer cette commande, l'event est désactivé !");
                return false;
            }

            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                BigTitle.sendTitleToPlayer(p, Utils.color(main.getConfigs().getEndMessageTitle()));
                BigTitle.sendSubtitleToPlayer(p, Utils.color(main.getConfigs().getEndMessage()));
            }
            main.setEventActivated(false);
        }

        if (args[0].equalsIgnoreCase("give")) {

            int id = Integer.parseInt(args[1]);

            if (!main.getItemID().isEmpty()) {
                player.sendMessage("§cVous avez déjà ajouté un item comme prochain loot.");
            }

            main.getItemID().add(id);
            player.sendMessage("§aVous avez bien ajouté l'item §e§l" + Material.getMaterial(id).name() + " §acomme prochain loot!");
        }


        return false;

    }


}

