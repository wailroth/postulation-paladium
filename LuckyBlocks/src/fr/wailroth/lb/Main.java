package fr.wailroth.lb;

import fr.wailroth.lb.config.CustomConfigFile;
import fr.wailroth.lb.listeners.OnBreak;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {


    FileConfiguration lbConfig;


    @Override
    public void onEnable() {
        init();
    }

    public void init() {
        registerFiles();
        registerEvents();
    }


    public void registerFiles() {
        CustomConfigFile customConfigFile = new CustomConfigFile(this);
        saveResource("lbconfig.yml", false);
        lbConfig = YamlConfiguration.loadConfiguration(customConfigFile.getFile("lbconfig"));
    }

    public void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new OnBreak(this), this);
    }

    public FileConfiguration getLbConfig() {
        return lbConfig;
    }

}
